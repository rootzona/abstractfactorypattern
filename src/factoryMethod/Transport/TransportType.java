package factoryMethod.Transport;

public enum TransportType {
    BUS_TRUCK,
    PLANE,
    SHIP
}
